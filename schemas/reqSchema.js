
const verifyReqSchema = {
    investorType: {
      in: ['body'],
      exists: {
        errorMessage: 'investorType missing'
      },
      notEmpty:{
        errorMessage: 'investorType should not be empty'
      },
      isString:{
        errorMessage: 'investorType should  be string'
      }
    },
    firstName: {
        in: ['body'],
        exists: {
          errorMessage: 'firtname missing'
        },
        notEmpty:{
          errorMessage: 'firtname should not be empty'
        },
        isString:{
          errorMessage: 'firtname should  be string'
        }
      },
      lastName: {
        in: ['body'],
        exists: {
          errorMessage: 'lastName missing'
        },
        notEmpty:{
          errorMessage: 'lastName should not be empty'
        },
        isString:{
          errorMessage: 'lastName should  be string'
        }
      },
      phone: {
        in: ['body'],
        exists: {
          errorMessage: 'phone missing'
        },
        notEmpty:{
          errorMessage: 'phone should not be empty'
        },
        isString:{
          errorMessage: 'phone should  be string'
        }
      },
      email: {
        in: ['body'],
        exists: {
          errorMessage: 'email missing'
        },
        notEmpty:{
          errorMessage: 'email should not be empty'
        },
        isString:{
          errorMessage: 'email should  be string'
        }
      },
      gender: {
        in: ['body'],
        exists: {
          errorMessage: 'gender missing'
        },
        notEmpty:{
          errorMessage: 'gender should not be empty'
        },
        isString:{
          errorMessage: 'gender should  be string'
        }
      },
      addressLine1: {
        in: ['body'],
        exists: {
          errorMessage: 'addressLine1 missing'
        },
        notEmpty:{
          errorMessage: 'addressLine1 should not be empty'
        },
        isString:{
          errorMessage: 'addressLine1 should  be string'
        }
      },
      country: {
        in: ['body'],
        exists: {
          errorMessage: 'country missing'
        },
        notEmpty:{
          errorMessage: 'country should not be empty'
        },
        isString:{
          errorMessage: 'country should  be string'
        }
      },
      state: {
        in: ['body'],
        exists: {
          errorMessage: 'state missing'
        },
        notEmpty:{
          errorMessage: 'state should not be empty'
        },
        isString:{
          errorMessage: 'state should  be string'
        }
      },
      city: {
        in: ['body'],
        exists: {
          errorMessage: 'city missing'
        },
        notEmpty:{
          errorMessage: 'city should not be empty'
        },
        isString:{
          errorMessage: 'city should  be string'
        }
      }
  }


  const downloadDocSchema = {
    key: {
      in: ['body'],
      exists: {
        errorMessage: 'key missing'
      },
      notEmpty:{
        errorMessage: 'key should not be empty'
      },
      isString:{
        errorMessage: 'key should  be string'
      }
    },
    Bucket: {
        in: ['body'],
        exists: {
          errorMessage: 'Bucket missing'
        },
        notEmpty:{
          errorMessage: 'Bucket should not be empty'
        },
        isString:{
          errorMessage: 'Bucket should  be string'
        }
      }
}



const createAccountSchema = {
  name: {
    in: ['body'],
    exists: {
      errorMessage: 'key missing'
    },
    notEmpty:{
      errorMessage: 'key should not be empty'
    },
    isString:{
      errorMessage: 'key should  be string'
    }
  },
  email: {
      in: ['body'],
      exists: {
        errorMessage: 'email missing'
      },
      notEmpty:{
        errorMessage: 'email should not be empty'
      },
      isEmail:{
        errorMessage: 'email should  be a valid email'
      }
    },
    password: {
      in: ['body'],
      exists: {
        errorMessage: 'password missing'
      },
      notEmpty:{
        errorMessage: 'password should not be empty'
      },
      isString:{
        errorMessage: 'password should  be string'
      }
    },
}

module.exports = {
    verifyReqSchema,
    downloadDocSchema,
    createAccountSchema
};