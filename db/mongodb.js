const config = require('../config/config');
const mongoose = require('mongoose');

const connectDB = (text) => {
try{
    mongoose.connect(config.dbUrl, {
        useNewUrlParser: true, useUnifiedTopology: true
     }).then(() => {
        console.log('Database connected Successfully');
     },
        (error) => {
           console.log('Error While Connecting Database: ', error)
        }
     );
}catch(e){
    throw {
        statusCode: 500,
        err: e['message']
    }
}
 
}

module.exports = {
    connectDB
};