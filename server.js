const express = require('express');
const cors = require("cors");
const app = express();
const investorRoutes = require('./routes/v1/investor.route');
const accountRoutes = require('./routes/v1/account.route');
const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('./swagger.json');
const path = require('path');
const {adminAuth} = require('./middleware/auth');
const {connectDB} =  require('./db/mongodb');
const port = 3000;

/*
* enable cors
*/
app.use(cors());

/*
* Connect MongoDB
*/
connectDB();

/*
* Swagger Api
*/
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));


/*
* Body parser
*/
app.use(express.urlencoded({ extended: true }))
app.use(express.json({ extended: true, limit: '10mb' }));

/*
* Routes
*/
app.use('/api/v1/investor', investorRoutes);
app.use('/api/v1/account', adminAuth, accountRoutes);


/*
* Http Header Authentication
*/
app.use(adminAuth);
app.use(express.static(path.join(__dirname, 'public')));

const server = app.listen(port, () => { console.log(`server started on port ${port}`) });

process.on('uncaughtException', (err) => {
   console.dir(err);
  });


