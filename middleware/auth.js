const config = require('../config/config');
const {encrypt, decrypt, getHash} = require('../utils/crypto');
const AccountModel = require('../models/acccount.model');

const userAuth = async (req, res, next) => {
    try{
        if(!req.headers['accesskeyid'] || !req.headers['secretaccesskey']){
            res.status(401).send({message: "Not Authorised"})
           }
        let accessKeyId = req.headers['accesskeyid'];
        let secretAccessKey = req.headers['secretaccesskey'];
        let hash = {accessKeyId,secretAccessKey}
        apiKey = decrypt(hash);
        let accountDeatils = await AccountModel.find({apiKey},{organisationId:1});
        if(!accountDeatils || accountDeatils.length < 1){
            res.status(401).send({message: "Not Authorised"});
        }
        if(accountDeatils[0] && accountDeatils[0]['organisationId']){
            //attaching organisationId to req
        req['organisationId'] = accountDeatils[0]['organisationId'];
        }
        next();
    }
catch(e){
    throw {
        statusCode: 400,
        err: e['message']
    }
}

};
 const adminAuth = (req, res, next) => {
    var authheader = req.headers.authorization;
    if (!authheader) {
        var err = new Error('You are not authenticated!');
        res.setHeader('WWW-Authenticate', 'Basic');
        err.status = 401;
        return next(err)
    }
 
    var auth = new Buffer.from(authheader.split(' ')[1],
    'base64').toString().split(':');
    var user = auth[0];
    var pass = auth[1];
 
    if (user == config.adminUsername && pass == config.adminPassword) {
       res.setHeader('Cache-Control', 'max-age=1');
        next();
    } else {
        var err = new Error('You are not authenticated!');
        res.setHeader('WWW-Authenticate', 'Basic');
        err.status = 401;
        return next(err);
    }
 
 }


module.exports = {
    adminAuth,
    userAuth
};