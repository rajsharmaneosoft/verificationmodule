import cv2
import numpy as np
import pytesseract
import matplotlib.pyplot as plt
import pandas as pd
from PIL import Image
import re

def display(img,cmap='gray'):
    fig = plt.figure(figsize=(12,10))
    ax = fig.add_subplot(111)
    ax.imshow(img,cmap='gray')

def run(doc):

    img = cv2.imread(doc)
    # plt.imshow(img)
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    # display(gray)
    th, threshed = cv2.threshold(gray, 127, 255, cv2.THRESH_TRUNC)
    # display(threshed)
    text1 = pytesseract.image_to_data(threshed, lang="eng",output_type='data.frame')
    text2 = pytesseract.image_to_string(threshed, lang="eng")

    text12 = pytesseract.image_to_data(gray, lang="eng",output_type='data.frame')
    text12 = pytesseract.image_to_string(gray, lang="eng")

    result = text2.split("\n")
    result= ",".join(result)
    result = result.translate({ord(i): None for i in '+-)(t>_�$~abcdefghijklmnopqrstuvwxyz'})
    result = result.replace(',', ' ')
    result  = ' '.join(result.split())

    result2 = text12.split("\n")
    result2= ",".join(result2)
    result2 = result2.translate({ord(i): None for i in '+-)(t>_�$~abcdefghijklmnopqrstuvwxyz'})
    result2 = result2.replace(',', ' ')
    result2  = ' '.join(result2.split())
    print(result2)
    print(result)

   
    

