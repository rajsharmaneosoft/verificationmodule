const express = require('express');
const router = new express.Router();
const investorController = require('../../controllers/investor.controller');
const util = require('../../utils/aws-s3');
const fs = require('fs');
const {userAuth} = require('../../middleware/auth');
const {  checkSchema } = require('express-validator');
const {verifyReqSchema, downloadDocSchema } = require('../../schemas/reqSchema');
const {expressValidatorCheck} = require('../../utils/util');

const verify = async (req, res, next) => {
    try {   
        expressValidatorCheck(req,res);
       let requestData = req.body;
       requestData['organisationId'] = req['organisationId'];
        let response = await investorController.verifyInvestor(requestData);
        res.status(200).send(response);
    } catch (err) {
        res.status(500).send(err['message'] || err)
    }
};



const downloadDocument = async (req, res, next) => {
    try {
        expressValidatorCheck(req,res);
        let requestData = req.body;
        let investorDetials = await investorController.findInvestors({$or:[ {'passportPicture':requestData}, {'internationalPassport':requestData}, {'drivingLicense':requestData} ]})
        if(investorDetials && investorDetials.length > 0){
            if(investorDetials[0]['organisationId'] != req['organisationId']){
                res.status(401).send({message: "Not Authorised"})
            }
            let filename = await util.downloadFile(requestData.key, requestData.Bucket);
            res.download(filename,filename, function(err){
                fs.unlinkSync(filename);
            });
        }else{
            res.status(400).send({message: "Bad Request"})
        }
    
    } catch (err) {
        res.status(500).send(err['message'] || err)
    }
};

//api not protected, it is just to test swagger
const getInvestorList = async (req, res, next) => {
    try {   
         let response = await investorController.findInvestors()
        res.status(200).send({data: response, message:"success"}); 
    } catch (err) {
        res.status(500).send(err['message'] || err)
    }
};




router.post('/verify', [userAuth, checkSchema(verifyReqSchema)],  verify);
router.post('/doc', [userAuth, checkSchema(downloadDocSchema)], downloadDocument);
router.get('/', getInvestorList);

module.exports = router;