const express = require('express');
const router = new express.Router();
const accountController = require('../../controllers/account.controller');
const {  check, validationResult } = require('express-validator');
const {decrypt, encrypt} = require('../../utils/crypto');
const {  checkSchema } = require('express-validator');
const {createAccountSchema } = require('../../schemas/reqSchema');
const {expressValidatorCheck} = require('../../utils/util');

const createAccount = async (req, res, next) => {
    try {
        expressValidatorCheck(req,res);
       let response = await accountController.createAccount(req.body);
      let apiKey="";
       if(response && response['data'] && response['data']['apiKey']){
        apiKey = encrypt(response['data']['apiKey']);
        res.status(200).send({message:"Signup Success", apiKey: apiKey});
       }else{
        res.status(500).send({message: "something wrong happened"});
       }  
       
       
    } catch (err) {
        res.status(500).send(err['message'] || err)
    }
};




router.post('/signup', checkSchema(createAccountSchema), createAccount);

module.exports = router;