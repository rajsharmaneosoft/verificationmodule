import sys
import face_recognition
import ocr 

doc = sys.argv[1]
known_image = face_recognition.load_image_file(doc)
ocr.run(doc)

unknown_image = face_recognition.load_image_file(sys.argv[2])


doc_encoding = face_recognition.face_encodings(known_image)[0]
unknown_encoding = face_recognition.face_encodings(unknown_image)[0]

results = face_recognition.compare_faces([doc_encoding], unknown_encoding)
# print(results)
if(results==[True]):
    print('facematched')
else:
    print('facedidnotmatch')

