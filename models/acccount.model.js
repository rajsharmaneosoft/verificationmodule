const mongoose = require('mongoose');
const Schema = mongoose.Schema;


let accountSchema = new Schema({
    organisationId: {
        type: String
    },
    name: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true,
        unique: true
    },
    password: {
      type: String
    },
    apiKey: {
     type: String
    },
    createdOn: {
        type: Date,
        default: Date.now
    },
    updatedOn: {
        type: Date,
        default: Date.now
    },
    createdOnUTCString: {
        type: String,
        default: new Date().toUTCString()
    },
    updatedOnUTCString: {
        type: String,
        default: new Date().toUTCString()
    }
})

module.exports = mongoose.model('account', accountSchema)