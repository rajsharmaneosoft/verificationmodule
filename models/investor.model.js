const mongoose = require('mongoose');
const Schema = mongoose.Schema;


var Doc = new Schema({
   key: String,
   Bucket: String
}, { _id : false });

let investorSchema = new Schema({
   organisationId: {
      type: String
   },
   isKycVerified: {
      type: Boolean,
      default: false
   },
   resonOfRejection: {
      type: String
   },
   investorType: {
      type: String,
      required: true
   },
   firstName: {
      type: String,
      required: true
   },
   lastName: {
      type: String,
      required: true
   },
   phone: {
      type: String,
      required: true
   },
   alternativePhone: {
      type: String
   },
   email: {
      type: String,
      required: true
   },
   profilePicture: Doc,
   gender: {
      type: String,
      required: true
   },
   addressLine1: {
      type: String,
      required: true
   },
   addressLine2: {
      type: String
   },
   country: {
      type: String,
      required: true
   },
   state: {
      type: String,
      required: true
   },
   city: {
      type: String,
      required: true
   },
   zipcode: {
      type: String
   },
   nationalIdCard: Doc,
   passportPicture: Doc,
   drivingLicense: Doc,
   proofOfAddress: Doc,
   internationalPassport: Doc,
   createdOn: {
      type: Date,
      default: Date.now
   },
   updatedOn: {
      type: Date,
      default: Date.now
   },
   createdOnUTCString: {
      type: String,
      default: new Date().toUTCString()
   },
   updatedOnUTCString: {
      type: String,
      default: new Date().toUTCString()
   }
})

module.exports = mongoose.model('investor', investorSchema)