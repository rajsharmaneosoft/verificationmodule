
const AccountModel = require('../models/acccount.model');
const { v4: uuidv4 } = require('uuid');
const { encrypt, getHash} = require('../utils/crypto');

const createAccount = async(params) => {
    try {
        params['organisationId'] = uuidv4();
        params['apiKey'] = getHash(params['email']);
        params['password'] = getHash(params['password']);
        let dataToSave = new AccountModel(params);
        let responseData = await dataToSave.save();
         return {statusCode: 200, status:"success", data: responseData};
             
    } catch (e) {
        throw {
            statusCode: 400,
            err: e['message']
        }
    }
}



module.exports = {
    createAccount
}