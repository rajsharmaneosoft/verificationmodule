
const InvestorModel = require('../models/investor.model');
const s3 = require('../utils/aws-s3');
const spawn = require("child_process").spawn;
const fs = require('fs');
const util = require('../utils/util');
const path = require('path');

const findInvestors = async (filter = {}) => {
    try {
        let data = await InvestorModel.find(filter);
        return  data;

    } catch (e) {
        throw {
            statusCode: 400,
            err: e['message']
        }
    }
}
const findInvestor = async (id) => {
    try {
        let data = await InvestorModel.findById(id);
        return { statusCode: 200, status: "success", data };

    } catch (e) {
        throw {
            statusCode: 400,
            err: e['message']
        }
    }
}

const verifyInvestor = async (investorDetails) => {
    try {
        process.on('uncaughtException', (err) => {
            util.deleteFiles([passportFilePath, facePhotoFlePath]); 
           });

        let base64_InternationalPassport = investorDetails['internationalPassport'];
        let base64_PassportPicture = investorDetails['passportPicture'];
        var facePhotoFlePath = await _convertBase64toFile(base64_PassportPicture);
        var passportFilePath = await _convertBase64toFile(base64_InternationalPassport);
        let {isKycVerified, resonOfRejection} = await _verifyKYC({ passport: passportFilePath, facePhoto: facePhotoFlePath, passportNo: investorDetails['passportNo'], firstName: investorDetails['firstName'], lastName: investorDetails['lastName'] });
        investorDetails['isKycVerified'] = isKycVerified;
      if(!isKycVerified){
        investorDetails['resonOfRejection'] = resonOfRejection;
      }

      util.deleteFiles([passportFilePath, facePhotoFlePath]);
        investorDetails['internationalPassport'] = await s3.uploadFile(base64_InternationalPassport);
        investorDetails['passportPicture'] = await s3.uploadFile(base64_PassportPicture);
        let dataToSave = new InvestorModel(investorDetails);
        let responseData = await dataToSave.save();
        return { statusCode: 200, status: "success", data: responseData };

    } catch (e) {
        util.deleteFiles([passportFilePath, facePhotoFlePath]);
        throw {
            statusCode: 400,
            err: e['message']
        }
    }
}

const _convertBase64toFile = async (b64String) => {
    try {
        return new Promise(async (resolve, reject) => {
            let base64String = b64String;
            let base64Data = new Buffer.from(base64String.replace(/^data:image\/\w+;base64,/, ""), 'base64');
            let extension = base64String.split(';')[0].split('/')[1];
            let fileName = Date.now() + "" + Math.floor(Math.random() * 100000000000000) + "." + extension;
            let filePath = path.join(__dirname, fileName)
            await fs.writeFileSync(filePath, base64Data, { encoding: 'base64' });
            resolve(filePath);
        })
    }
    catch (e) {
        throw {
            statusCode: 400,
            err: e['message']
        }
    }
    
}

const _verifyKYC = async (params) => {
        return new Promise((resolve, reject) => {
            let process = spawn('python3', ["face.py", params['passport'], params['facePhoto']]);
            process.stdout.on('data', function (data, err) {
                if (err) {
                reject(err);
                }
                let kycResult = {};
                let filteredString = data.toString().replace(/(\r\n|\n|\r|<br>|<b>|<|>|h3)/gm, " ").replace(/\s/g, "");
                filteredString = filteredString.toLowerCase();
                let isNameMatched = filteredString.includes(params['firstName'].toLowerCase()) && filteredString.includes(params['lastName'].toLowerCase());
                let isPassportNoMatched = filteredString.includes(params['passportNo'].toLowerCase());
                let isFacematched = filteredString.includes('facematched');   
                let isKycVerified = isNameMatched && isPassportNoMatched && isFacematched;
                let resonOfRejection = null;
                if(!isKycVerified){
                    let reasonArr = [];
                    kycResult['resonOfRejection'] = "";
                    if(!isNameMatched){
                        reasonArr.push("Name");
                    }
                    if(!isPassportNoMatched){
                        reasonArr.push("Passport No");;
                    }
                    if(!isFacematched){
                        reasonArr.push("Face");;
                    }
                     resonOfRejection = reasonArr.join(",").replace(/,(?=[^,]*$)/, ' and ') + " did not match";
                     

                }
                resolve({isKycVerified, resonOfRejection});
            });

        }).catch(err => {
            util.deleteFiles([params['passport'], params['facePhoto']]);
        });
}

module.exports = {
    findInvestors,
    findInvestor,
    verifyInvestor
}