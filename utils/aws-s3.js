const AWS = require('aws-sdk');
const logger = require("./log");
const fs = require('fs');
const config = require('../config/config');

AWS.config.update({
    accessKeyId: config.awsAccessKeyId,
    secretAccessKey: config.awsSecretAccessKey,
    region: config.awsRegion
});

const s3 = new AWS.S3();

const uploadFile = async (file) => {
    try {
        let base64String = file;
        const base64Data = new Buffer.from(base64String.replace(/^data:image\/\w+;base64,/, ""), 'base64');

        // Getting the file extension
        const type = base64String.split(';')[0].split('/')[1];

        const params = {
            Bucket: "ktpc",
            Key: `idcards/` + Date.now() + "" + Math.floor(Math.random() * 100000000000000) + `.${type}`,
            Body: base64Data,
            ACL: 'private',
            ContentEncoding: 'base64',
            ContentType: type,
            SSECustomerAlgorithm: 'AES256',
            SSECustomerKey: config.SSECustomerKey
        }
        return await s3.upload(params).promise();

    } catch (err) {
        logger.error(err.message, err);
        throw (err.message);
    }
}

const downloadFile = async (key, bucketName) => {
    try {
        let fileName = key.split('/').pop();
        const params = {
            Bucket: bucketName,
            Key: key,
            SSECustomerAlgorithm: 'AES256',
            SSECustomerKey: config.SSECustomerKey
        }
        let res = await s3.getObject(params).promise();
        var data = new Buffer.from(res.Body);
        await fs.writeFileSync(fileName, data, 'binary');
        return fileName;
    } catch (err) {
        logger.error(err.message, err);
        throw (err.message);
    }
}

module.exports = {
    uploadFile,
    downloadFile
};