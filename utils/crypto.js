const crypto = require('crypto');
const config = require('../config/config');
const algorithm = 'aes-256-ctr';
const secretKey = config.cipherSecretKey;
const iv = crypto.randomBytes(16);

const encrypt = (text) => {
    try {
        const cipher = crypto.createCipheriv(algorithm, secretKey, iv);
        const encrypted = Buffer.concat([cipher.update(text), cipher.final()]);

        return {
            accessKeyId: iv.toString('hex'),
            secretAccessKey: encrypted.toString('hex')
        };
    } catch (e) {
        throw {
            statusCode: 400,
            err: e['message']
        }
    }

};

const decrypt = (hash) => {
    try {
        const decipher = crypto.createDecipheriv(algorithm, secretKey, Buffer.from(hash.accessKeyId, 'hex'));

        const decrpyted = Buffer.concat([decipher.update(Buffer.from(hash.secretAccessKey, 'hex')), decipher.final()]);

        return decrpyted.toString();
    }
    catch (e) {
        throw {
            statusCode: 400,
            err: e['message']
        }
    }

};

const getHash = (text) => {
    try {
        const secret = config.hashKey;
        const hash = crypto.createHmac('sha256', secret).update(text).digest('hex');
        return hash
    }
    catch (e) {
        throw {
            statusCode: 400,
            err: e['message']
        }
    }

}


module.exports = {
    encrypt,
    decrypt,
    getHash
};