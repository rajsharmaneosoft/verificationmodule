const fs = require('fs');
const path = require('path');
const {   validationResult} = require('express-validator');


const deleteFiles = (filePathArray) => {
try{
    if(!Array.isArray(filePathArray)){
return;
    }
    filePathArray.forEach((filePath) => {
        if(fs.existsSync(filePath)){
            fs.unlinkSync(filePath);
        } 
        
    });

}catch(e){
    throw {
        statusCode: 400,
        err: e['message'] || e 
    }
}
 
};

const expressValidatorCheck = (req,res) =>{
    const errors = validationResult(req);
    if(!errors.isEmpty()){
       throw errors;
    }
}


module.exports = {
    deleteFiles,
    expressValidatorCheck
};